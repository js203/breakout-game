const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

let x = canvas.width / 2;
let y = canvas.height - 30;

// Приращение мяча
let dx = 2;
let dy = -2;

// Приращение ракетки
let dpx = 7;

const ballRadius = 10;

const paddleHeight = 10;
const paddleWidth = 75;
let paddleX = (canvas.width - paddleWidth) / 2; // Левая сторона ракетки

let rightPressed = false;
let leftPressed = false;

// Параметры кирпичей
const bricks = [];
const brickRowCount = 3;
const brickColumnCount = 5;
const brickWidth = 75;
const brickHeight = 20;
const brickPadding = 10; // Расстояние между кирпичами
const brickOffsetTop = 30; // Смещение всех кирпичей сверху
const brickOffsetLeft = 30; // Смещение всех кирпичей слева
let bricksCount = brickColumnCount*brickRowCount;

let score = 0;
let lives = 3;

let willGameOver = false; // Указывает, что нужно завершить игру

let animationFrame;

function init() {
    // Инициализация событий
    document.addEventListener("keydown", keyDownHandler, false);
    document.addEventListener("keyup", keyUpHandler, false);
    document.addEventListener("mousemove", mouseMoveHandler, false);

    // Инициализация кирпичей
    for (let c = 0; c < brickColumnCount; c++) {
        bricks[c] = [];
        for (let r = 0; r < brickRowCount; r++) {
            const brickX = c * (brickWidth + brickPadding) + brickOffsetLeft;
            const brickY = r * (brickHeight + brickPadding) + brickOffsetTop;
            bricks[c][r] = { x: brickX, y: brickY, status: 1, score: 1 };
        }
    }
}

function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function drawBricks() {
    for (let c = 0; c < brickColumnCount; c++) {
        for (let r = 0; r < brickRowCount; r++) {
            if (bricks[c][r].status === 1) {
                ctx.beginPath();
                ctx.rect(bricks[c][r].x, bricks[c][r].y, brickWidth, brickHeight);
                ctx.fillStyle = "#0095DD";
                ctx.fill();
                ctx.closePath();
            }
        }
    }
}

function drawScore() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.fillText(`Score: ${score}`, 8, 20);
}

function drawLives() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.fillText(`Lives: ${lives}`, canvas.width - 65, 20);
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBall();
    drawPaddle();
    drawBricks();
    drawScore();
    drawLives();
    gameOver();
    x += dx;
    y += dy;
    hittingDetection();
    collisionDetection();
    if (rightPressed) {
        paddleX = Math.min(paddleX += dpx, canvas.width - paddleWidth);
    } else if (leftPressed) {
        paddleX = Math.max(paddleX -= dpx, 0);
    }

    animationFrame = requestAnimationFrame(draw);
}

function keyDownHandler(e) {
    if (e.key === "Right" || e.key === "ArrowRight") {
        rightPressed = true;
    } else if (e.key === "Left" || e.key === "ArrowLeft") {
        leftPressed = true;
    }
}

function keyUpHandler(e) {
    if (e.key === "Right" || e.key === "ArrowRight") {
        rightPressed = false;
    } else if (e.key === "Left" || e.key === "ArrowLeft") {
        leftPressed = false;
    }
}

function mouseMoveHandler(e) {
    const relativeX = e.clientX - canvas.offsetLeft;
    if (relativeX > paddleWidth/2 && relativeX < canvas.width-paddleWidth/2) {
        paddleX = relativeX - paddleWidth / 2;
    }
}

// Проверка попадания в стенки или ракетку
function hittingDetection() {
    if (x + dx + ballRadius > canvas.width || x + dx < ballRadius) {
        dx = -dx;
    }
    if (y + dy < ballRadius) {
        // Отражение от верхней стенки
        dy = -dy;
    } else if (y + dy + ballRadius > canvas.height) {
        // Попадание в нижнюю стенку
        if (x > paddleX && x < paddleX+paddleWidth) {
            // Попадание в ракетку
            dy = -dy;
        } else {
            // Мимо ракетки
            lives = lives-1;
            if (!lives) {
                willGameOver = true;
            } else {
                // Новый раунд начинаем со стартовой позиции мяча и ракетки
                x = canvas.width / 2;
                y = canvas.height - 30;
                dx = 2;
                dy = -2;
                paddleX = (canvas.width - paddleWidth) / 2;
            }
        }
    }
}

// Проверка попадания в кирпичи
function collisionDetection() {
    for (let c = 0; c < brickColumnCount; c++) {
        for (let r = 0; r < brickRowCount; r++) {
            const b = bricks[c][r];
            if (
                b.status === 1 &&
                x > b.x &&
                x < b.x+brickWidth &&
                y > b.y &&
                y < b.y+brickHeight
            ) {
                dy = -dy;
                b.status = 0;
                score = score + b.score;
                bricksCount = bricksCount-1;
                if (bricksCount === 0) {
                    alert('Kissi');
                    willGameOver = true;
                }
            }
        }
    }
}

function gameOver() {
    if (willGameOver) {
        cancelAnimationFrame(animationFrame);
    }
}

init();
draw();
